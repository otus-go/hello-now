package main

import (
	"fmt"
	"os"
	"strconv"
	"time"

	"github.com/beevik/ntp"
)

// Config - struct for application config
type Config struct {
	Format   string
	Server   string
	Interval time.Duration
}

// AppConfig - application config instance
var AppConfig = Config{
	Format:   "15:04:05.000",
	Server:   "1.ru.pool.ntp.org",
	Interval: 5 * time.Second,
}

// GetLocalTime returns string representation of current time
func GetLocalTime(format string) string {
	return time.Now().Format(format)
}

// GetNtpTime returns string representation of current time from NTP server
func GetNtpTime(server, format string) string {
	time, err := ntp.Time(server)
	if err != nil {
		return "n/a"
	}
	return time.Format(format)
}

func init() {
	if format, ok := os.LookupEnv("FORMAT"); ok {
		AppConfig.Format = format
	}

	if server, ok := os.LookupEnv("SERVER"); ok {
		AppConfig.Server = server
	}

	if interval, ok := os.LookupEnv("INTERVAL"); ok {
		duration, err := strconv.Atoi(interval)
		if err != nil {
			panic("Interval value must be an integer")
		}
		AppConfig.Interval = time.Duration(duration) * time.Second
	}
}

func main() {
	for {
		ntp := GetNtpTime(AppConfig.Server, AppConfig.Format)
		local := GetLocalTime(AppConfig.Format)
		fmt.Printf("%-15s %s\n", "Ntp time:", ntp)
		fmt.Printf("%-15s %s\n", "Computer time:", local)
		time.Sleep(AppConfig.Interval)
	}
}
